# GitLab / GitLab Workflow

GitLab is a web-based git repository manager platform which manages software development projects and it's files, as they are changing over time. Git stores this information in a structured data called a repository. It provides version control, issue tracking and CI/CD pipilines.

# Why GitLab ?

There several other git repository platfoms such as Github, BitBucket etc. But the question arises Why Gitlab ? The answer to this depends on the project requirements and the ease of workflow in either platforms. 

* GitLab comes with free, powerfull and well-integrated Continuous Integration (CI) and Continuous Deployement (CD) pipeline. Other platform offer manual and paid CI service (Circle CI Github).

* Free Container Registry i.e built-in docker registry for each repository.

* Kubernetes Integration, Here's a link to [integration](https://about.gitlab.com/solutions/kubernetes/). 

* Better Project management, GitLab is generally serves better for product companies since it has better issue tracking, boards, milestones for Merge requests and shorter Cycle time.

* Shorter Cycle times : By shorter cylce time it means that the whole cycle of steps from managing to create becomes less. It includes :
    * Manage
    * Plan
    * Create
    * Verify
    * Release
    * Configure
    * Monitor
    * Secure

* Built In Security, It refers to testing the code changes as it being merged. In GitLab, security is first class. Security checks are built into merge requests (MRs), with problems being reported directly in pipelines and MRs. Some of our integrated security features include:

    * Dependency scanning
    * Static Application Security Testing (SAST)
    * Dynamic Application Security Testing
    * Container scanning


# Alternatives to GitLab 


GitLab is one the most popular Git-Repository manangement platform.
There are some other platform which allows self hosting git management or public repo management: 

1. GitHub
2. Bitbucket
3. SourceForge
4. Gogs
5. Phabricator
6. Gitea

# How much time or patience you need to finish this course.


This course requires an average of 1.5 hours to complete.

# Any prerequisites for this course

Before starting this course you must have the basic knowledge of version control and Git.

* [Git](https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F)
* [Version-Control](https://www.atlassian.com/git/tutorials/what-is-version-control)

# Gitlab WorkFlow Demonstration (Hello World)


There are few terminologies in a repository in **GitLab** that you need to understand in this course. A particular repositories contains following terminologies/Sections. Some important ones are :

* Project Overview
* Repository
* Issues
* Merge Requests 
* CI / CD
* Analytics
* Settings
* Metrics

### Project Overview

Provides in overall overview the the project which includes.

1. Details
2. Activity 
3. Releases

### Repository

This sections contains the information related to the repository which generally includes

1. Files
2. Commits
3. Branches
4. Contributers
5. Locked Files
6. Compare

### Issues

This section includes the list of the issues in the repository. It includes sub sections :

1. List
2. Labels : All the labels present in the repo.
3. Milestones
4. Boards : Includes `To-Dos` and `Doing`

### Merge Requests

This section contains the list of all the merge requests that are either in progress or are resolved and to be reviewed.

### CI / CD

This section helps with the Continuous Integration and Continuous Deployement of the projects. Includes services like **pipeline**, **schedulers** and  **job**.

### Analytics

This section shows the analytics of the repository.
It includes analytics of :

* Issues
* CI /CD
* Code Review

### Settings

This section manages the setting related to the repository/project. The categories are :

* General
* Integrations
* Members
* Repository
* Webooks
 
and more.

### Operations

This sections generally deals the deployement services like : **Kubernetes**, **Metrics**, **Tracing of the deployement**, **Logs**, **serverless**.

## How to create an Issue in GitLab ?


Creating an issue in GitLab is super easy.

1. Go to Issue section
2. Click on New Issue
3. Add title, description, assign to you or relevant developer and Press the Submit button. 

There you go, The issue has been created. **BUT** I'm sure there were no guidelines on how to create a good issue.

We at Smarter.Codes strongly recommends these guidelines. 

* The title of the issue should be precise.
* To create a good issue, the description of the issue should be put in a elaborating manner. Try to always include :
    
    * **WHAT ?** : This should give a brief of the issue i.e this slould explain what is the issue, what might be the reason of it's cause.

    * **WHY ?** : This should explain that the **why** do we want this change or **why** do we want to fix this issue. Explain it's benefits.

    * **HOW ?** : This would explain that **how** we want to address this problem. Providing probable solutions to the issue would be really great. This would help us to have a better understanding of the issue and the solution. 


After adding the description, one can add `Assignee`, `Milestone`, `Labels` and there's an option to add `Due Date` too.

**Step 1** : *Creating an Issue*

![](/images/newIssue.png)

**Step 2** : *Adding Title*

![](/images/newIssueTitle.png)

**Step 3** : *Editing Description*

![](/images/newIssueDiscription.png)

**Step 4** : *Adding Assignees, labels, tags, milestones etc.*

![](/images/newIssueLabels.png)

**Step 5** : *Click on **SUBMIT** and the issue is created.*

![](/images/newIssueCreated.png)

Here's the link to [GitlLab's guide](https://help.github.com/en/github/managing-your-work-on-github/creating-an-issue)

### What to do after creating an issue?

Once you create an issue, you must be wondering what now? Let's start resolving.

So GitLab provides a very clean and simple workflow.

#### Creating a merge request in GitLab.

After creating an issue, open the particular issue. On the right side you'll see a button/dropdown `Create merge request`. From there you can a create a branch by choosing the source branch.

Now you must be that you could've created the branch locally and started working on it. Yes, definitely but we strongly recommend creating branches from GitLab and reason for that is ;

1. GitLab will give a proper name to branch including issue number and adding the issue title to it. This would help in better issue tracking.

2. Branches created will always be latest from the source branch.

3. It will automatically link the merge request to issue, which improves the issue management.

4. It will close the issue itself after the related merge request has been merged.

**Creating a Merge Request**

![](/images/newIssueCreated.png)

The above image contains a drop down menu. 
  * Select the option i.e Create merge request and branch / Create branch.
  * Choose the resource branch.
  * let the GitLab decide the branch name.
  * Click on submit and Merge request is created.
  
You free to create as many branches as you and start working on them.

### After creating a merge request

Once you create a merge request.

Fetch the changes locally by running `git fetch`. You'll see that the new branch has been fetched.

Now you need to checkout to the branch by `git checkout <BranchName>` and start making changes.

#### **[Commit messages should be appropriate and it should give an idea of its purpose](https://www.freecodecamp.org/news/writing-good-commit-messages-a-practical-guide/)**.

This what commits on branchs looks like. 

![](/images/newCommits.png)

### Guidelines to follow while working on a branch

* Push your commits regularly i.e on every hour to branch. 

* If you're researching and not able to make regular commits. Make sure to add comments on the MR of your progress. This will help us to track the progress better.

* Once you've resolved the issue. Make sure to add screenshots of the changes or just make a small video demonstrating the changes done. 

* Please resolve the WIP (Work in Progress) if the merge request is ready to merge.

* Tag the reviewer and notify them that the merge request is ready for the review.


**Note : You're free to make branches and make commits/changes for the issues that are assigned to you.**

Once everything is reviewed and good to go.

Congrats your work has been merged. Thank you very much for your contribution.








